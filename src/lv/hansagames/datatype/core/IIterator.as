package lv.hansagames.datatype.core 
{
	
	 /**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public interface IIterator 
	{
		function contains(data:*):Boolean;
		function hasNext():Boolean;
		function hasPrevious():Boolean;
		
		function next():*;
		function previous():*;
		function reset():void;
		function destroy():void;
		function set source(value:*):void
	}

}
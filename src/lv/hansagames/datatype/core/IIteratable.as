package lv.hansagames.datatype.core 
{
	
	 /**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public interface IIteratable 
	{
		function get iterrator():IIterator;
	}

}
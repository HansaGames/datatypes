package lv.hansagames.datatype.dlinkedlist 
{
	
	 /**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public interface IDoubleLinkedLIstNode 
	{
		function get id():String;
		function set id(value:String):void;
		
		function get data():*;
		function set data(value:*):void;
		
		function get previous():IDoubleLinkedLIstNode;
		function set previous(value:IDoubleLinkedLIstNode):void;
		
		function get next():IDoubleLinkedLIstNode;
		function set next(value:IDoubleLinkedLIstNode):void;
		
		function destroy():void;
	}

}
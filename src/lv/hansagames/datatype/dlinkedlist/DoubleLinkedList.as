package lv.hansagames.datatype.dlinkedlist
{
	import lv.hansagames.datatype.core.IIteratable;
	import lv.hansagames.datatype.core.IIterator;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class DoubleLinkedList implements IDoubleLinkedList, IIteratable
	{
		private var _head:IDoubleLinkedLIstNode;
		private var _tail:IDoubleLinkedLIstNode;
		private var _currentPointer:IDoubleLinkedLIstNode;
		private var _length:uint;
		
		public function DoubleLinkedList()
		{
			_head = _tail = null;
			_length = 0;
		}
		
		/* INTERFACE lv.hansagames.datatype.dlinkedlist.IDoubleLinkedList */
		
		public function get head():IDoubleLinkedLIstNode
		{
			return _head;
		}
		
		public function get tail():IDoubleLinkedLIstNode
		{
			return _tail;
		}
		
		public function addNodeAtBegining(data:IDoubleLinkedLIstNode):void
		{
			if (data)
			{
				if (_head)
				{
					data.next = _head;
					_head.previous = data;
					_head = data;
				}
				else
				{
					_currentPointer = _head = _tail = data;
				}
				
				_length++;
			}
		}
		
		public function addDataAtBegining(data:*):void
		{
			var node:IDoubleLinkedLIstNode = new DoubleLinkedListNode();
			node.data = data;
			addNodeAtBegining(node);
		}
		
		public function addNodeAtEnd(data:IDoubleLinkedLIstNode):void
		{
			if (data)
			{
				if (_tail)
				{
					_tail.next = data;
					data.previous = tail;
					_tail = data;
				}
				else
				{
					_currentPointer = _head = _tail = data;
				}
				_length++;
			}
		}
		
		public function addDataAtEnd(data:*):void
		{
			var node:IDoubleLinkedLIstNode = new DoubleLinkedListNode();
			node.data = data;
			addNodeAtEnd(node);
		}
		
		public function removeHead():IDoubleLinkedLIstNode
		{
			var temp:IDoubleLinkedLIstNode;
			
			if (_head)
			{
				temp = _head;
				
				_head = temp.next;
				
				if (_head)
				{
					_head.previous = null;
				}
				
				temp.next = null;
				_length--;
				return temp;
			}
			
			return null;
		}
		
		public function removeTail():IDoubleLinkedLIstNode
		{
			var temp:IDoubleLinkedLIstNode;
			if (_tail)
			{
				temp = _tail;
				_tail = temp.previous;
				
				if (_tail)
				{
					_tail.next = null;
				}
				
				temp.previous = null;
				_length--;
				return temp;
			}
			
			return null;
		}
		
		public function removeNode(node:IDoubleLinkedLIstNode):*
		{
			if (node == _head)
			{
				return removeHead().data;
			}
			else if (node == _tail)
			{
				return removeTail().data;
			}
			else
			{
				node.previous.next = node.next;
				node.next.previous = node.previous;
				_length--;
				node.previous = null;
				node.next = null;
				return node.data;
			}
			return null
		}
		
		public function addNodeAfter(data:IDoubleLinkedLIstNode, node:IDoubleLinkedLIstNode):void
		{
			if (data && node && data != node && contains(node))
			{
				if (node == _tail)
				{
					addNodeAtEnd(data)
				}
				else
				{
					data.next = node.next;
					node.next.previous = data;
					data.previous = node;
					node.next = data;
					_length++;
				}
			}
		}
		
		public function addDataAfter(data:*, node:IDoubleLinkedLIstNode):void
		{
			var newNode:IDoubleLinkedLIstNode = new DoubleLinkedListNode();
			newNode.data = data;
			addNodeAfter(newNode, node);
		}
		
		public function addNodeBefore(data:IDoubleLinkedLIstNode, node:IDoubleLinkedLIstNode):void
		{
			if (data && node && data != node && contains(node))
			{
				if (node == _head)
				{
					addNodeAtBegining(data);
				}
				else
				{
					node.previous.next = data;
					data.previous = node.previous;
					data.next = node;
					node.previous = data;
					_length++;
				}
			}
		}
		
		public function addDataBefore(data:*, node:IDoubleLinkedLIstNode):void
		{
			var newNode:IDoubleLinkedLIstNode = new DoubleLinkedListNode();
			newNode.data = data;
			addNodeBefore(newNode, node);
		}
		
		public function contains(data:*):Boolean
		{
			return iterrator.contains(data);
		}
		
		public function removeAll():void
		{
			var itterator:IIterator = iterrator;
			var node:IDoubleLinkedLIstNode
			while (iterrator.hasNext())
			{
				node = iterrator.next();
				
				if (node)
				{
					removeNode(node);
					node.destroy();
				}
			}
		}
		
		public function get iterrator():IIterator
		{
			return new DoubleLinkedListIterrator(this);
		}
		
		public function get currentPointer():IDoubleLinkedLIstNode
		{
			return _currentPointer;
		}
		
		public function set currentPointer(value:IDoubleLinkedLIstNode):void
		{
			_currentPointer = value;
		}
		
		public function get length():uint
		{
			return _length;
		}
	
	}

}
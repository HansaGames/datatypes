package lv.hansagames.datatype.dlinkedlist
{
	import lv.hansagames.datatype.core.IIterator;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class DoubleLinkedListIterrator implements IIterator
	{
		private var _source:IDoubleLinkedList;
		private var _currentPointer:IDoubleLinkedLIstNode;
		
		public function DoubleLinkedListIterrator(source:IDoubleLinkedList)
		{
			_source = source;
		}
		
		/* INTERFACE lv.hansagames.datatype.core .IIterator */
		
		public function hasNext():Boolean
		{
			if (_source)
			{
				if (!_currentPointer)
				{
					return (_source.head != null);
				}
				
				if (_currentPointer)
				{
					if (_source.currentPointer)
					{
						return (_currentPointer.next && _currentPointer.next != _source.currentPointer);
					}
					else
					{
						return (_currentPointer != _source.tail);
					}
				}
			}
			return false;
		}
		
		public function hasPrevious():Boolean
		{
			if (_source)
			{
				if (!_currentPointer)
				{
					return (_source.head != null);
				}
				
				if (_currentPointer)
				{
					if (_source.currentPointer)
					{
						return (_currentPointer.previous && _currentPointer.previous != _source.currentPointer);
					}
					else
					{
						return (_currentPointer.previous != _source.head);
					}
				}
			}
			return false;
		}
		
		public function next():*
		{
			if (hasNext())
			{
				if (!_currentPointer)
				{
					_currentPointer = _source.head;
					return _currentPointer;
				}
				
				_currentPointer = _currentPointer.next;
				return _currentPointer;
			}
			return null;
		}
		
		public function previous():*
		{
			if (hasPrevious())
			{
				if (!_currentPointer)
				{
					_currentPointer = _source.head;
					return _currentPointer;
				}
				_currentPointer = _currentPointer.previous;
				return _currentPointer;
			}
			return null;
		}
		
		public function reset():void
		{
			_currentPointer = null;
		}
		
		public function destroy():void
		{
			if (_source)
			{
				_source = null;
			}
		}
		
		public function contains(data:*):Boolean 
		{
			var node:IDoubleLinkedLIstNode;
			while (hasNext()) 
			{
				node = next();
				if (node && (node.data == data || node==data) )
					return true;
			}
			
			return false;
		}
		
		public function set source(value:*):void
		{
			if (value is IDoubleLinkedList)
			{
				reset();
				_source = value;
			}
		}
	
	}

}
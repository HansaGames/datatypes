package lv.hansagames.datatype.dlinkedlist 
{
	import lv.hansagames.datatype.core.IIteratable;
	
	 /**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public interface IDoubleLinkedList extends IIteratable
	{
		
		function get head():IDoubleLinkedLIstNode;
		function get tail():IDoubleLinkedLIstNode;
		
		function addNodeAtBegining(data:IDoubleLinkedLIstNode):void;
		function addDataAtBegining(data:*):void;
		
		function addNodeAtEnd(data:IDoubleLinkedLIstNode):void;
		function addDataAtEnd(data:*):void;
		
		function addNodeAfter(data:IDoubleLinkedLIstNode,node:IDoubleLinkedLIstNode):void;
		function addDataAfter(data:*, node:IDoubleLinkedLIstNode):void;
		
		function addNodeBefore(data:IDoubleLinkedLIstNode,node:IDoubleLinkedLIstNode):void;
		function addDataBefore(data:*,node:IDoubleLinkedLIstNode):void;
		
		function removeHead():IDoubleLinkedLIstNode;
		function removeTail():IDoubleLinkedLIstNode;
		function removeNode(node:IDoubleLinkedLIstNode):*;
		function removeAll():void;
		
		function get length():uint;
		
		function get currentPointer():IDoubleLinkedLIstNode;
		function set currentPointer(value:IDoubleLinkedLIstNode):void;
		
		function contains(data:*):Boolean;
	}

}
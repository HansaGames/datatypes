package lv.hansagames.datatype.dlinkedlist 
{
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class DoubleLinkedListNode implements IDoubleLinkedLIstNode
	{
		private var _data:*;
		private var _id:String;
		private var _previous:IDoubleLinkedLIstNode;
		private var _next:IDoubleLinkedLIstNode;
		
		public function DoubleLinkedListNode() 
		{
			
		}
		
		/* INTERFACE lv.hansagames.datatype.dlinkedlist.IDoubleLinkedLIstNode */
		
		public function destroy():void 
		{
			_data = null;
			_id = '';
			_previous = null;
			_next = null;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		
		public function get previous():IDoubleLinkedLIstNode 
		{
			return _previous;
		}
		
		public function set previous(value:IDoubleLinkedLIstNode):void 
		{
			_previous = value;
		}
		
		public function get next():IDoubleLinkedLIstNode 
		{
			return _next;
		}
		
		public function set next(value:IDoubleLinkedLIstNode):void 
		{
			_next = value;
		}
		
		public function get data():* 
		{
			return _data;
		}
		
		public function set data(value:*):void 
		{
			_data = value;
		}
		
	}

}